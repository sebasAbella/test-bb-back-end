
# Reto Banco de Bogotá 

Este proyecto fue generado en [Express](https://expressjs.com/es/starter/installing.html) ; con intención del desarrollo de prueba técnica para el Banco de Bogotá.  

## Development server

* Ejecutar proyecto :

```
OPCION 1 => DOCKER:
Para ejecutar el proyecto se debe tener instalado `Node.js`y `Docker` opcional `Nodemon`.
- 'docker build -t test-bb-back-end .' (crear imagen de la ruta actual)
- 'docker run -it -p 4000:4000 test-bb-back-end' (ejecuta imagen en otro puerto it modo iterativo)
```

```
OPCION 2 => NODEMON:
Para ejecutar el proyecto se debe tener instalado `Node.js` y `Nodemon`.
-  ejecutar 'npm i' (instalar dependencias)
- ejecutar 'nodemon src/index.js' (se puede ejecutar cambios y actualizarse)
```


## Librerias implementadas

* Cors
* sequelize
* mysql2

## Copyright 

* Author: Sebastian Abella Rocha
* Cel : 3195275807