module.exports = (sequelize, type) => {
  return sequelize.define('person', {
    fullname: type.STRING,
    birth: type.DATEONLY,
  });
};
