const router = require('express').Router();

const { Person } = require('../../../db');

router.get('/', async (req, res) => {
  const persons = await Person.findAll();
  res.json(persons);
});

router.post('/', async (req, res) => {
  const person = await Person.create(req.body);
  res.json(person);
});

router.put('/:personId', async (req, res) => {
  await Person.update(req.body, {
    where: { id: req.params.personId },
  });
  res.json({ succes: 'se ha modificado registro.' });
});

router.delete('/:personId', async (req, res) => {
  await Person.destroy({
    where: { id: req.params.personId },
  });
  res.json({ succes: 'se ha eliminado registro.' });
});

module.exports = router;
