const router = require('express').Router();
const apiPersonRouter = require('./api/person');

router.use('/person', apiPersonRouter);

module.exports = router;
