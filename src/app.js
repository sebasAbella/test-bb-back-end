const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
const apiRouter = require('./routes/api');
const app = express();

app.use(cors());

require('../db');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use('/api', apiRouter);

module.exports = app;
